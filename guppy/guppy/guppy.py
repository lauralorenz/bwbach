import os
import subprocess
import logging
import re

import tensorflow as tf
import gpt_2_simple as gpt2

from guppy.discord import Channel

MODEL_NAME = os.environ.get("GPT2_MODEL", "124M")
MODEL_DIR = "/models"

logger = logging.getLogger(__name__)


class GPTClient(object):
    def __init__(self, model=MODEL_NAME):

        self.prefix = None
        logger.info(f"Checking for existence of {MODEL_NAME}")
        if not gpt2.is_gpt2_downloaded(model_name=model, model_dir=MODEL_DIR):
            logger.info("Model does not exist. Downloading now.")
            self.get_model()

    def get_model(self):
        if not os.path.isdir(os.path.join("models", MODEL_NAME)):
            print(f"Downloading {MODEL_NAME}...")
            gpt2.download_gpt2(
                model_dir=MODEL_DIR, model_name=MODEL_NAME
            )  # model is saved into current directory under MODEL_DIR/124M/

    def load_model(self, checkpoint="latest", model_name=None):
        logger.info(f"Loading {model_name} into memory.")
        gpt2.load_gpt2(
            self.session, model_name=model_name, model_dir=MODEL_DIR
        )

    def finetune(self, file_name):
        self.session = gpt2.start_tf_sess()
        self.load_model(checkpoint=None, model_name=MODEL_NAME)
        gpt2.finetune(
            self.session,
            file_name,
            steps=1000,
            overwrite=True,
            model_dir=MODEL_DIR,
        )  # steps is max number of training steps

    def _truncate_text_response(self, text, word_limit=50):
        counter = 0
        keep = list()
        text_list = text.split(" ")

        while counter < int(word_limit):
            try:
                word = text_list[counter]
                keep.append(word)
                counter +=1
            except IndexError:
                break

        rejoined = " ".join(keep)
        total_length = len(rejoined)
        pop_index = total_length
        for idx, char in enumerate(rejoined[::-1]):
            if re.match(r"(\.|\n)", char):
                rejoined = rejoined[0:pop_index-1]
                break
            else:
                pop_index = total_length - idx
        return rejoined

    def _pop_prefix_from_response(self, text):

        return text.replace(self.prefix, '', 1)


    def generate(self, text, word_limit=50):
        tf.keras.backend.clear_session()
        self.session = gpt2.start_tf_sess()
        self.prefix = text
        self.load_model(checkpoint=None, model_name=MODEL_NAME)
        text =  gpt2.generate(
            self.session,
            prefix=text,
            return_as_list=True,
            model_dir=MODEL_DIR,
            model_name=MODEL_NAME,
        )
        # return text
        text = text[0]
        text = self._pop_prefix_from_response(text)
        text = self._truncate_text_response(text, word_limit)
        text = text.strip()

        return text





    def dump_discord_chats(self, channel: Channel):
        previous_listing = set(os.listdir("/data"))
        logger.info("Pulling chats from Discord.")
        subprocess.check_output(["/opt/dce/pull_discord.sh", channel.value])
        new_listing = set(os.listdir("/data"))

        return new_listing.difference(previous_listing)
