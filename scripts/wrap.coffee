send_direct_message = (robot, user_id, message) ->
  data = {"room": "#{user_id}"}
  robot.send data, message

send_to_channel = (robot, room_id, message) ->
  data = {"room": "#{room_id}"}
  robot.send data, message

# getAllFuncs = (toCheck) ->
#     props = []
#     obj = toCheck
#     props = props.concat(Object.getOwnPropertyNames(obj))
#     while obj = Object.getPrototypeOf(obj)
#       props = props.concat(Object.getOwnPropertyNames(obj))

#     z= props.sort().filter(
#       (e, i, arr) -> 
#         if (e!=arr[i+1] && typeof toCheck[e] == 'function')
#           return true
#     )


module.exports = {
    send_direct_message,
    send_to_channel
    # getAllFuncs
}


   
