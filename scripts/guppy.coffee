
# Description:
#   <description of the scripts functionality>
#
# Dependencies:
#   "<module name>": "<module version>"
#
# Configuration:
#   LIST_OF_ENV_VARS_TO_SET
#
# Commands:
#   hubot <trigger> - <what the respond trigger does>
#   <trigger> - <what the hear trigger does>
#
# Notes:
#   <optional notes required for the script>
#
# Author:
#   <github username of the original script author>



checkForCommand = (commands, input) ->
  inputCommand = input.split(" ")[1]
  return unless inputCommand

   
  for command in commands
    exampleCommand = command.replace /[-<].*$/, ''
    continue if exampleCommand.match("^hubot *$") or not exampleCommand.match("^hubot")
    command = exampleCommand.split(" ")[1]
    continue unless command


    if inputCommand == command
      console.log "is command: true"
      return true

  console.log "is command: false"
  return false

talkingtoBwbach = (message_text) ->
  istalking= /^bwbach/i.test(message_text)
  console.log "is talking: #{istalking}"
  return istalking



module.exports = (robot) ->

  robot.respond /_reset guppy/i, (msg) ->
    robot.brain.set "running_guppy", false
    msg.send "Guppy is reset."

  robot.catchAll (msg) ->

    message_text = msg.message.text
    if talkingtoBwbach(message_text)
      iscommand = checkForCommand(robot.commands, message_text)
      if not iscommand
        running_guppy = robot.brain.get("running_guppy") 
        if running_guppy
          msg.send "Hold on. Still thinking."
        else
          robot.brain.set 'running_guppy', true

          message_text = message_text.replace /bwbach/, ''
          message_text = message_text.replace /\s{2,}/, ' '

          user = process.env.GUPPY_USER
          pass = process.env.GUPPY_PWD
          auth = 'Basic ' + new Buffer(user + ':' + pass).toString('base64')
          requestData = JSON.stringify({
          text: message_text
          })
          msg.send "Let me think about that for a minute."
          robot.http("http://guppy:8811/generate?word_limit=#{process.env.GUPPY_MAX_LEN}")
          .headers(Authorization: auth, Accept: 'application/json')
          .post(requestData) (err, response, body) ->
            try
              data = JSON.parse body
              msg.send data
            catch error
              msg.send "Sorry, I don't know much about that."
            robot.brain.set "running_guppy", false

    msg.finish()
    # for command in robot.commands
    #   msg.send "#{command}"
    # message = msg.message
    # message.text = message.text or ''
    # showSuggestions(robot.commands, message, robot.adapter, robot.name) if message.text.match ///^@?#{robot.name}\ +.*$///i
