FROM node:10
RUN apt-get update && apt-get install -y vim
RUN npm install -g yo generator-hubot hubot-discord
RUN useradd -ms /bin/bash bwbach_user
USER bwbach_user
WORKDIR /home/bwbach_user

RUN mkdir bwbach
WORKDIR /home/bwbach_user/bwbach

RUN yo hubot --owner="jim" --name="bwbach" --description="fyh" --adapter="discord" --defaults 
RUN npm install hubot-redis-brain --save
RUN npm install hubot-reload-scripts --save
RUN npm install hubot-table-flip --save
RUN npm install hubot-markov --save
RUN npm install hubot-uptimerobot --save
COPY ./external-scripts.json .

# for some reason, the hubot-markov script is broken here
# dunno if its the adapter or what, but copying
# over this custom, tiny modified version of one of its source 
# code files should fix it.
COPY ./markov/default-listeners.coffee /home/bwbach_user/bwbach/node_modules/hubot-markov/src/default-listeners.coffee


USER root
COPY ./scripts/* ./scripts/
RUN chown -R bwbach_user:bwbach_user ./scripts/
USER bwbach_user

ENTRYPOINT [ "./bin/hubot"]
CMD ["--adapter", "discord"]