FROM ubuntu:18.04

RUN apt-get update && apt-get install -y wget

# install .NET core
RUN wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN apt-get install -y software-properties-common

RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update && apt-get install -y apt-transport-https dotnet-runtime-3.1 unzip python3.7 python3-pip python3.7-dev build-essential


# install discord scraping tool from this dude. thanks dude!
RUN wget https://github.com/Tyrrrz/DiscordChatExporter/releases/download/2.20/DiscordChatExporter.CLI.zip
RUN mkdir /opt/dce
RUN unzip DiscordChatExporter.CLI.zip -d /opt/dce
RUN mkdir /data

COPY ./guppy/requirements.txt ./requirements.txt
RUN pip3 install -r ./requirements.txt
COPY ./guppy/guppy/pull_discord.sh /opt/dce/pull_discord.sh
COPY ./guppy/guppy /opt/guppy

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8=value
COPY ./guppy/app.py /opt/app.py
WORKDIR /opt
ENTRYPOINT [ "uvicorn" ]
CMD ["--port", "8811", "--host", "0.0.0.0", "--workers", "1", "app:app"]